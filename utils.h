#ifndef UTILS_H
#define UTILS_H

#define CANT_JUGS 2

typedef enum {
	VACIO = 0,
	AZUL,
	ROJO,
	MORADO,
	NEGRO,
	BLANCO,
} Color;

unsigned sumarFila(const unsigned arr[]);
int imprimirAzulejo(int azCol);

#endif
