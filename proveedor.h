#ifndef PROVEEDOR_H
#define PROVEEDOR_H

#include <stdbool.h>
#include "utils.h"

#define CANT_MOST ((CANT_JUGS * 2) +1)
#define FIL_AZU (CANT_MOST + 3)

typedef struct {
	unsigned azulejos[FIL_AZU][5];
	bool marcadorTurno;
} Proveedor;

bool escogerAzulejos(Proveedor * partPtr, Color * azColPtr, unsigned * azCanPtr);
bool proveedorVacio(const unsigned azs[][5]);
void surtirMostradores(Proveedor * partPtr);
void iniciarPartida(Proveedor * partPtr);

#endif
