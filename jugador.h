#ifndef JUGADOR_H
#define JUGADOR_H

#include "utils.h"
#include <stdbool.h>

typedef struct {
	Color colorAzulejo[5];
	unsigned cantidad[5];
} Patrones;

extern const Color MOSAICO[5][5];

typedef struct {
	char nombre[20];
	unsigned puntos;
	Patrones lineas;
	Color pared[5][5];
	unsigned piso;
	bool primerTurno;
} Jugador;

void calcularPuntajeFinal(Jugador jugs[]);
bool calcularPuntaje(Jugador jugs[], Color basu[]);
void imprimirResultados(const Jugador jugs[]);
void agregarPenalizacion(unsigned * piso, const unsigned pena);
unsigned ubicarAzulejos(const Color azCol, unsigned azCan,
		Patrones * patrPtr, Color pared[][5], unsigned * pisoPtr);
void imprimirTablero(const Jugador * jug);
unsigned escogerIniciadorRonda(Jugador jugs[]);
void iniciarJugadores(Jugador jugs[]);
void impAzulejos(const Color azCol, const unsigned azCan);

#endif
