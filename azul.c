#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "utils.h"
#include "proveedor.h"
#include "jugador.h"

static void jugarRonda(Jugador jugs[], Proveedor * partPtr, unsigned iJug);

const Color MOSAICO[5][5] = {
	{ AZUL, ROJO, MORADO, NEGRO, BLANCO },
	{ BLANCO, AZUL, ROJO, MORADO, NEGRO },
	{ NEGRO, BLANCO, AZUL, ROJO, MORADO },
	{ MORADO, NEGRO, BLANCO, AZUL, ROJO },
	{ ROJO, MORADO, NEGRO, BLANCO, AZUL }
};

int main(void){
	Proveedor partida;
	Jugador jugadores[CANT_JUGS];

	srand(time(NULL));

	iniciarPartida(&partida);
	iniciarJugadores(jugadores);

	unsigned jugadorInicial;

	do {
		surtirMostradores(&partida);
		partida.marcadorTurno = true;
		jugadorInicial = escogerIniciadorRonda(jugadores);
		jugarRonda(jugadores, &partida, jugadorInicial);
	} while (!calcularPuntaje(jugadores,partida.azulejos[1]));

	calcularPuntajeFinal(jugadores);
	imprimirResultados(jugadores);

	exit(EXIT_SUCCESS);
}

static void jugarRonda(Jugador jugs[], Proveedor * partPtr, unsigned iJug){
	puts("\nInicio de ronda...");

	while (!proveedorVacio((const unsigned (*)[])partPtr->azulejos)){
		Color azCol = VACIO;
		unsigned azCan = 0, azDes = 0;

		printf("\nTurno de jugador %u - %s\n", iJug + 1,
				jugs[iJug].nombre);

		printf("Tablero de jugador %u - %s", iJug + 1,
				jugs[iJug].nombre);
		imprimirTablero(&jugs[iJug]);

		puts("\nProveedor:");
		if (escogerAzulejos(partPtr, &azCol, &azCan)){
			jugs[iJug].primerTurno = true;
			agregarPenalizacion(&jugs[iJug].piso, 1);
		}

		printf("\nAzulejos a ubicar: ");
		impAzulejos(azCol, azCan);

		azDes = ubicarAzulejos(azCol, azCan, &jugs[iJug].lineas,
				jugs[iJug].pared, &jugs[iJug].piso);

		if (azDes > 0){
			partPtr->azulejos[1][azCol - 1] += azDes;
		}

		printf("Tablero de jugador %u - %s", iJug + 1,
				jugs[iJug].nombre);
		imprimirTablero(&jugs[iJug]);

		char c;
		do {
		printf("%s", "Desea continuar (S: sí, N: no): ");
		scanf(" %c", &c); // Capturo enter
		} while ( c != 's' && c != 'S' && c != 'N' && c != 'n');

		if (c == 'N' || c == 'n'){
			calcularPuntajeFinal(jugs);
			imprimirResultados(jugs);
			exit(EXIT_SUCCESS);
		}

		iJug =  (iJug + 1) % CANT_JUGS;
	}

	puts("\nFin de ronda...");
}
