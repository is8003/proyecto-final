#include <stdio.h>
#include <stdlib.h>
#include "utils.h"

unsigned sumarFila(const unsigned arr[]){
	unsigned sum = 0;

	for (size_t i = 0; i < 5; i++){
		sum += arr[i];
	}

	return sum;
}

int imprimirAzulejo(int azCol){
	switch (azCol){
		case 0:
			printf("%s", "|_|");
			break;
		case 1:
			printf("%s", "|A|");
			break;
		case -1:
			printf("%s", "|a|");
			break;
		case 2:
			printf("%s", "|R|");
			break;
		case -2:
			printf("%s", "|r|");
			break;
		case 3:
			printf("%s", "|M|");
			break;
		case -3:
			printf("%s", "|m|");
			break;
		case 4:
			printf("%s", "|N|");
			break;
		case -4:
			printf("%s", "|n|");
			break;
		case 5:
			printf("%s", "|B|");
			break;
		case -5:
			printf("%s", "|b|");
			break;
		default:
			fprintf(stderr, "ERROR - imprimirAzulejo: %d no "\
					"representa un color de azulejo "\
					"válido.\n", azCol);
			return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
