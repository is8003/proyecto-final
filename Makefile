.PHONY: clean

TARGET = azul

CC = gcc
CFLAGS = -std=c17 -pedantic -Wall -Wextra -g
TEST_CFLAGS = $(CFLAGS) -lrt -lm

OUTDIR = ./bin
OBJDIR = ./obj
TESTDIR = ./test

all: utils.o proveedor.o jugador.o $(TARGET)

test: test_utils test_proveedor test_jugador

azul: $(OBJDIR)/jugador.o $(OBJDIR)/proveedor.o $(OBJDIR)/utils.o azul.c
	$(CC) $(CFLAGS) $^ -o $(OUTDIR)/$@

jugador.o: jugador.c
	$(CC) $(CFLAGS) -c $^ -o $(OBJDIR)/$@

proveedor.o: proveedor.c
	$(CC) $(CFLAGS) -c $^ -o $(OBJDIR)/$@

utils.o: utils.c
	$(CC) $(CFLAGS) -c $^ -o $(OBJDIR)/$@

test_jugador: $(TESTDIR)/test_jugador.c utils.c
	$(CC) $(TEST_CFLAGS) $^ -o $(OUTDIR)/$@

test_proveedor: $(TESTDIR)/test_proveedor.c utils.c
	$(CC) $(TEST_CFLAGS) $^ -o $(OUTDIR)/$@

test_utils: $(TESTDIR)/test_utils.c
	$(CC) $(TEST_CFLAGS) $^ -o $(OUTDIR)/$@

clean :
	rm -fv $(OUTDIR)/*  $(OBJDIR)/*
